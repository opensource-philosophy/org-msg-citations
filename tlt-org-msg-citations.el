(require 'org-msg)
(require 'org-ref)

(defgroup tlt-org-msg nil
  "Export `org-ref' citation links when sending a mail with `org-msg'."
  :group 'tlt-utils)

(defcustom tlt-org-msg-citations t
  "Whether org-ref-citations should be handled correctly by org-msg."
  :group 'tlt-utils)

(defcustom tlt-org-msg-bibliography-heading "Literature"
  "Bibliography heading to be inserted after the mail."
  :group 'tlt-org-msg)

(defcustom tlt-org-msg-bibliography-colon t
  "Whether `tlt-org-msg-bibliography-heading' should end with a colon.
  Relevant for bold-face or italicized names."
  :group 'tlt-org-msg)

(defcustom tlt-org-msg-csl-file org-ref-csl-default-style
  "Style file to be used to create inline-citations
  and the bibliography."
  :group 'tlt-org-msg)

(defcustom tlt-org-msg-export-keybindings t
  "Whether or not to export string wrapped in tildes '~' as keybindings.
    If set to non-nil, export '~foo~' as '<kbd>foo</kbd>'.")

(defcustom tlt-org-msg-syntax-highlight-css t
      "Whether or not syntax highlighting colors are set manually in a 'css' file.")

(defvar tlt-org-msg-parent-data nil
  "Internal variable to store the value `mu4e-compose-parent-message' while in `mu4e-compose-mode'.")

(defun tlt-org-msg-colon ()
  "If `tlt-org-msg-bibliography-colon' is non-nil,
return the string containing only a colon.
Else return the empty string."
  (if tlt-org-msg-bibliography-colon ":" ""))

(defun tlt-org-msg-bibliography-link ()
  "Return an `org-ref'-compatible bibliography string."
  (if (stringp bibtex-completion-bibliography)
      bibtex-completion-bibliography
    (string-join
     bibtex-completion-bibliography ",")))

(defcustom tlt-org-msg-bibliography-string-html
  ;; 'display:inline-block' needed for 'margin-bottom' to work! ;;
  (format
   "
@@html:<hr style=\"margin-bottom:0px;margin-top:12px\">@@@@html:<span style=\"text-decoration:underline;font-weight:bold;line-height:10px;margin-top:12px;margin-bottom:12px;display:inline-block;\">%s@@@@html:</span>%s@@[[bibliography:%s]]
      " tlt-org-msg-bibliography-heading (tlt-org-msg-colon) (tlt-org-msg-bibliography-link))
  "String with which to insert the `org-ref' bibliography link for HTML mails.
The link is inserted in the line following the last word of the message.
See `tlt-org-msg-add-bibliography-string'."
  :group 'tlt-org-msg)

(defcustom tlt-org-msg-bibliography-string-ascii
  (format
   "-----
%s%s\n\n[[bibliography:%s]]
-----" tlt-org-msg-bibliography-heading (tlt-org-msg-colon) (tlt-org-msg-bibliography-link))
  "String with which to insert the `org-ref' bibliography link for plain text mails.
The link is inserted in the line following the last word of the message.
See `tlt-org-msg-add-bibliography-string'."
  :group 'tlt-org-msg)

;;; Functions to add the bibliography link ;;;

(defun tlt-org-ref-check-citelinks ()
  "Return non-nil if at least one citation was used in the mail."
  (org-element-map (org-element-parse-buffer) 'link
    (lambda (lnk)
      (when (assoc (org-element-property :type lnk) org-ref-cite-types)
        lnk))))

(defun tlt-org-msg-save-parent ()
  "Store the message being replied to or forwarded in `tlt-org-msg-parent-data'."
  (setq tlt-org-msg-parent-data mu4e-compose-parent-message))

(defun tlt-org-msg-formatted-citation-line ()
  "Return the citation line string if `message-citation-line-function' is `message-insert-formatted-citation-line'.
      For messages which are neither replies nor forwarded, return an unmatchable regexp (needed for `tlt-org-msg-find-string' to return nil)."
        (if tlt-org-msg-parent-data
  (let ((date (message-make-date             ; make that :date field an emacs-legible string
               (mu4e-message-field           ; get the :date field, which is a list of numbers like (25313 28399 0)
                tlt-org-msg-parent-data      ; the list with all the relevant parent message data
                :date)))
        (from (plist-get                                                ; plist contains :name and :e-mail
               (car (mu4e-message-field tlt-org-msg-parent-data :from)) ; because the plist is in a singleton list
               :name)))
    (with-temp-buffer
      (message-insert-formatted-citation-line from date)
      (buffer-substring-no-properties (point-min) (point-max))))
        regexp-unmatchable))

(defun tlt-org-msg-simple-citation-line ()
  (concat (message-fetch-field "from") " writes:"))

(defun tlt-org-msg-find-string (string)
  "Search for the line containing only STRING (with possible whitespaces to the left) and return the position of its beginning."
  (save-excursion
    (goto-char (point-min))
    (let* ((re (concat "^[:space:]*" (regexp-quote string)))
           (end (re-search-forward re nil t))
           (begin (re-search-backward re nil t)))
      begin)))

(defun tlt-org-msg-find-citation-line (list)
  "From LIST consisting of occurrences of numbers or nil, return the smallest number.
        If all members of LIST are nil, return nil."
  (let ((result))
    (dolist (pos list result)
      (unless (null pos)
        (push pos result)))
    (if (null result)
        (point-max)
      (apply 'min result))))

(defun tlt-org-msg-end ()
  "Return the point of the beginning of the message body."
  (save-excursion
    (let*
        (;; "<name> <mail> writes:" ;;
         (simple-citation-line (tlt-org-msg-simple-citation-line))
         ;; see `message-citation-line-format' ;;
         (formatted-citation-line (tlt-org-msg-formatted-citation-line))
         ;; "Citation follows this line" ;;
         (simple-line (tlt-org-msg-find-string simple-citation-line))
         (formatted-line (tlt-org-msg-find-string formatted-citation-line))
         (org-msg-line (tlt-org-msg-find-string org-msg-separator))

         (position (tlt-org-msg-find-citation-line (list simple-line formatted-line org-msg-line))))
      position)))

(defun tlt-org-msg-prepare-reply ()
  "Prepare everything for the bibliography string to be added before a citation line."
  (goto-char (tlt-org-msg-end))             ; go to the end of the message
  (re-search-backward "[^[:space:]]$")
  (forward-char)
  (delete-blank-lines)
  (newline 2)) ; blank line between end of mail and bibliography string

(defun tlt-org-msg-prepare-no-reply ()
  "Prepare everything for the bibliography string to be added if there is no citation."
  (goto-char (tlt-org-msg-end))             ; go to the end of the message
  (delete-blank-lines)                      ; if there are blank lines, remove them
  (fixup-whitespace)                        ; remove any whitespace
  (newline 2))                              ; to have one blank line

(defun tlt-org-msg-insert-separator ()
  "Insert <hr> tag at point."
  (insert "@@html:<hr style=\"margin-bottom:15px;margin-top:15px\">@@"))

(defcustom tlt-org-msg-bib-sep
  "@@html:<hr style=\"margin-bottom:-10px;margin-top:-2px\">@@"
  "Separator for replies.")

(defun tlt-org-msg-add-bibliography-string (string)
  "If the mail uses citations, add an (org-ref-bibliography) STRING to the end of it. If it is a reply, add it before the citation of the mail to which it is replied and separate it from the reply-message with a horizontal line. If it is not a reply, add it at the end of the message and do not insert a lower hoizontal line."
  (when (tlt-org-ref-check-citelinks)
    (let ((reply-or-forward (tlt-org-msg-end)))
      (save-excursion
        (if (not (equal reply-or-forward (point-max)))
            (progn
              (tlt-org-msg-prepare-reply)
              (insert (concat string tlt-org-msg-bib-sep)))
          (tlt-org-msg-prepare-no-reply)
          (insert string))
        ;; to make sure you always get the same number of blank lines ;;
        (when (looking-at "\n[ \t]*$")
          (delete-blank-lines))
        (newline)))))

(defun tlt-org-msg-add-bibliography-string-ascii (_)
  "Add the bibliography string for plain text mails
 at the end of the message or before the first citation."
  (tlt-org-msg-add-bibliography-string tlt-org-msg-bibliography-string-ascii))

(defun tlt-org-msg-add-bibliography-string-html (_)    ; and return nil (because of the hook)
  "Add the bibliography string for HTML mails at the end of the message or before the first citation."
  (tlt-org-msg-add-bibliography-string tlt-org-msg-bibliography-string-html))

(defun tlt-org-msg-preprocess-ascii (_)
  "Export `org-ref' citations in the current buffer to ASCII."
  (let ((org-ref-csl-default-style tlt-org-msg-csl-file))
    (org-ref-csl-preprocess-buffer 'ascii)))

(defun tlt-org-msg-preprocess-html (_)
  "Export all `org-ref' citations to HTML style citations."
  (let ((org-ref-csl-default-style tlt-org-msg-csl-file))
    (org-ref-csl-preprocess-buffer 'html)))

(defun tlt-org-msg-export-as-ascii (fun charset str)
  "Transform the Org STR into ASCII plain text.
Advice for org-msg-export-as-text."
  (let* ((hook org-export-before-parsing-hook)
         (new-hook (append hook
                           '(tlt-org-msg-add-bibliography-string-ascii
                             tlt-org-msg-preprocess-ascii)))
         (org-export-before-parsing-hook new-hook))
    (funcall fun charset str)))

;; (defun tlt-org-msg-export-as-html (fun str)
;;   "Transform the Org STR into HTML text.
;; Advice for `org-msg-export-as-html'."
;;   (let* ((hook org-export-before-parsing-hook)
;;          (new-hook (append hook '(tlt-org-msg-add-bibliography-string-html tlt-org-msg-preprocess-html)))
;;          (org-export-before-parsing-hook new-hook))
;;     (funcall fun str)))

(defun tlt-org-msg-kbd-tags (verbatim _contents info)
  "Wrap text in VERBATIM object with HTML kbd tags.
The kbd wrapping is done if `org-hugo-use-code-for-kbd' is non-nil.

CONTENTS is nil.  INFO is a plist used as a communication
channel."
      (format "<kbd>%s</kbd>" (org-html-encode-plain-text
                               (org-element-property :value verbatim))))

(defun tlt-org-msg-export-keybindings ()
  (if tlt-org-msg-export-keybindings
`((code . tlt-org-msg-kbd-tags)
        (special-block . org-msg--html-special-block)
                           (quote-block . org-msg--html-quote-block)
                           ,@(org-export-get-all-transcoders 'html))
                '((special-block . org-msg--html-special-block)
                           (quote-block . org-msg--html-quote-block)
                           ,@(org-export-get-all-transcoders 'html))))


(defun tlt-org-msg-export-as-html (str)
  "Transform the Org STR into html."
  (let* ((hook org-export-before-parsing-hook)
         (new-hook (append hook '(tlt-org-msg-add-bibliography-string-html tlt-org-msg-preprocess-html)))
         (org-export-before-parsing-hook new-hook))
  (prog2
      (org-export-define-derived-backend 'org-msg-html 'html
        :translate-alist (tlt-org-msg-export-keybindings))
      (org-msg-xml-to-str (org-msg-build str))
    (setq org-export-registered-backends
          (cl-delete-if (apply-partially 'eq 'org-msg-html)
                        org-export-registered-backends
                        :key 'org-export-backend-name)))))

(defun tlt-org-msg-org-to-xml (str &optional base)
  "Like `org-msg-org-to-xml' but allow deciding the alue of `org-html-htmlize-output-type'."
  (save-window-excursion
    (with-temp-buffer
      (insert str)
      (when org-msg-convert-citation
        (org-msg-ascii-blockquote 0 (point-min-marker) (point-max-marker)))
      (let ((org-html-table-default-attributes nil)
            (org-html-htmlize-output-type (if tlt-org-msg-syntax-highlight-css 'css 'inline-css))
            (org-html-head-include-scripts nil)
            (org-html-head-include-default-style nil)
            (org-msg-export-in-progress t))
        (let ((buf (generate-new-buffer-name " *OrgMsg HTML Export*")))
          (with-current-buffer (org-export-to-buffer 'org-msg-html buf)
            (let ((xml (org-msg-html-buffer-to-xml base)))
              (kill-buffer)
              xml)))))))

;;;###autoload
(define-minor-mode tlt-org-msg-citations-mode
  "Get citations exported correctly when using `org-msg-mode'. Also depends on `org-ref'."
  :init-value nil
  (if tlt-org-msg-citations-mode                                                     ; if the minor mode is turned on
      (progn                                                                         ; then do the following:
        (require 'org-ref)                                                           ; load org-ref
        (advice-add #'org-msg-org-to-xml :override #'tlt-org-msg-org-to-xml)
        (advice-add #'org-msg-export-as-text :around #'tlt-org-msg-export-as-ascii)  ; advice the ascii export function and
        (advice-add #'org-msg-export-as-html :override #'tlt-org-msg-export-as-html))   ; the html export function of org-msg

    (advice-remove #'org-msg-export-as-text #'tlt-org-msg-export-as-ascii)
    (advice-remove #'org-msg-org-to-xml #'tlt-org-msg-org-to-xml)))          ; these advices!

(provide 'tlt-org-msg-citations)

;; tlt-org-msg-citations.el ends here
