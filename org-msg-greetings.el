(defcustom edgecase-list '("a very long lastname")
  "List of edge case lastnames.")

(defcustom user-firstname "Vitus"
  "First name of the user.")

(defcustom user-lastname "Schäfftlein"
  "Last name of the user.")

(defcustom frame-functions '(frame-formal frame-semi-formal frame-default)
  "List of Functions which return an address.")

(defcustom addressbook "/home/vitus/org-msg-greetings.csv"
  "Path to the file storing information about how to address the recipients.")

(defun get-recipient-to-field ()
  "Return a list containing the recipient's mail address and, if provided, their name."
  (split-string (org-msg-message-fetch-field "to") " <" t))

(defun recipient-name-p ()
  "Return non-nil if the recipient's name is provided."
  (equal (length (get-recipient-to-field)) 2))

(defun get-recipient-mail ()
  "Get e-mail-address from recipient."
  (let* ((recname (recipient-name-p))
         (mail-raw (if recname
                       (nth 1 (get-recipient-to-field))
                     (nth 0 (get-recipient-to-field))))
         (len (if recname
                  (1- (length mail-raw))
                (length mail-raw))))
    (substring mail-raw 0 len)))

(defun get-recipient-name ()
  "Get full name from recipient. If provided, return it. If not, try to guess using the mail."
  (if (recipient-name-p)
      (car (get-recipient-to-field))))

(defun split-recipient-name-quotation-marks (recname)
"Split a string by commas after removing quotation marks if there are exactly two quotation marks."
(let ((num-quotes (count ?\" recname)))
  (if (= num-quotes 2)
      (let ((cleaned-recname (replace-regexp-in-string "\"" "" recname)))
        (reverse (split-string cleaned-recname ",")))
    nil)))

(defun split-recipient-name-noble ()
  "Search for a name containing 'von' or 'van'.
        If it is found, return a list containing, if existent, the first name, and the noble last name.
        Return nil if not."
  (goto-char (point-min))
  (when (re-search-forward "\\(^\\|\s\\)v[oa]n.*$" nil t); finding "von/van der X"
    (let ((beg (match-beginning 0))
          (end (match-end 0)))
      (if (equal beg 1)
          (list (buffer-substring-no-properties beg end))
        (list
         (buffer-substring-no-properties (point-min) beg)
         (buffer-substring-no-properties (1+ beg) end))))))

(defun split-recipient-name-edgecase ()
  "Search for an edgecases using `edgecase-list'.
       If it is found, return a list containing, if existent, the first name, and the edgecase last name.
Return nil if not."
  (let (result)
    (dolist (edgecase edgecase-list result)
      (goto-char (point-min))
      (when (re-search-forward
             (regexp-opt (list edgecase)) nil t)
        (let ((beg (match-beginning 0))
              (end (match-end 0)))
          (setq result
                (if (equal beg 1)
                    (list (buffer-substring-no-properties beg end))
                  (list
                   (buffer-substring-no-properties (point-min) (1- beg))
                   (buffer-substring-no-properties beg end)))))
        result))))

(defun split-recipient-name-default ()
  "Search for a usual one-word last name and return it."
  (goto-char (point-max))
  (let ((result-raw (re-search-backward "\s" nil t)))
    (if result-raw
        (list (buffer-substring-no-properties (point-min) (point))
              (buffer-substring-no-properties (1+ (point)) (point-max)))
      (list (buffer-substring-no-properties (point-min) (point-max))))))

(defun names-from-mail ()
  "If in the recipient's mail address, '.' occurs before '@', return a list of the first and last name, initials upcased.
  Else, return a list of the whole mail address, initials upcased, until, exluding '@'."
  (let ((mail (get-recipient-mail)))
    (with-temp-buffer
      (insert mail)
      (goto-char (point-min))
      (if (re-search-forward "[[:alpha:]]+\\.[[:alpha:]]+@" nil t) ; if there is a dot in the mail address before "@"
          (progn
            (let* ((end (1- (match-end 0)))
                   (name (buffer-substring-no-properties (point-min) end)))
              (mapcar #'upcase-initials
                      (split-string name "\\."))))
        (goto-char (point-min))
        (re-search-forward "@")
        (list (upcase-initials
               (buffer-substring-no-properties
                (point-min) (1-
                             (match-end 0)))))))))

(defun firstname-from-mail ()
  "Return the first name extracted from the mail address, or `nil'."
  (let ((names-mail (names-from-mail)))
    (when (equal (length names-mail) 2)
      (nth 0 names-mail))))

(defun lastname-from-mail ()
  "Return the last name extracted from the mail address.
      If unsuccessful, return the whole mail address until '@'."
  (let ((names-mail (names-from-mail)))
    (if (equal (length names-mail) 2)
        (nth 1 names-mail)
      (nth 0 names-mail))))

(defun split-recipient-name ()
  "Return a list containing first and last name of the recipient, if possible.
If a name is provided, split it by edgecase, 'van'/'von' or by the last
whitespace. If no name is provided, try to get it from the mail address."
  (let ((recname (get-recipient-name)))
    (if recname
        (let ((recipient-name-list
               (or (split-recipient-name-quotation-marks recname)
                   (progn
                     (with-temp-buffer
                       (insert recname)
                       (or
                        (split-recipient-name-edgecase)       ; edge cases
                        (split-recipient-name-noble)          ; von/van
                        (split-recipient-name-default)))))))
          (mapcar #'string-trim recipient-name-list))
      nil)))

(defun recipient-firstname-p ()
  "Return non-nil if the recipient's name is provided."
  (equal (length (split-recipient-name)) 2))

(defun recipient-firstname ()
  "Return the recipient's first name, if existent, or prompt for it, using the mail address as a guess."
  (if (recipient-firstname-p)
      (nth 0 (split-recipient-name))
    (read-string "First name: " (firstname-from-mail))))

(defun recipient-lastname ()
  "Return the recipient's last name, or, or prompt for it, using the mail address as a guess."
  (let ((recname (split-recipient-name)))
    (if (recipient-firstname-p)
        (nth 1 recname)
      (read-string "Last name: " (lastname-from-mail)))))

(defun ask-for-gender (firstname lastname)
  "Ask the user for the recipient's gender and return it."
  (let* ((name (if firstname
                   (concat firstname " " lastname)
                 lastname))
         (answer
          (read-multiple-choice
           (format "What gender is the recipient %s?" name)
           '((?f "female" "The recipient's gender is female.")
             (?m "male" "The recipient's gender is male."))))
         (gender (nth 1 answer)))
    gender))

(defun female-p (gender)
  (equal gender "female"))

(defun beginning-formal (extra-r form-of-address lastname)
  (format "Liebe%s %s %s,\n\n" extra-r form-of-address lastname))

(defun end-formal (user-firstname user-lastname)
  (format "Mit besten Grüßen,\n%s %s\n" user-firstname user-lastname))

(defun end-semi-formal (user-firstname user-lastname)
  (format "Herzliche Grüße,\n%s %s\n" user-firstname user-lastname))

(defun beginning-default (extra-r recipient-firstname)
  (format "Liebe%s %s,\n\n" extra-r recipient-firstname))

(defun end-default (user-firstname)
  (format "Herzliche Grüße,\n%s\n" user-firstname))

(defun frame-formal (gender firstname lastname)
  "Return a list of a greeting string and a farewell string - formal."
  (let* ((female (female-p gender))
         (form-of-address (if female "Frau" "Herr"))
         (extra-r (if female "" "r"))
         (beginning (beginning-formal extra-r form-of-address lastname))
         (end (end-formal user-firstname user-lastname)))
    (list beginning end)))

(defun frame-semi-formal (gender firstname lastname)
  "Return a list of a greeting string and a farewell string - semi-formal."
  (let* ((female (female-p gender))
         (form-of-address (if female "Frau" "Herr"))
         (extra-r (if female "" "r"))
         (beginning (beginning-formal extra-r form-of-address lastname)) ; same as formal
         (end (end-semi-formal user-firstname user-lastname)))
    (list beginning end)))

(defun frame-default (gender firstname lastname)
  "Return a list of a greeting string and a farewell string - default."
  (let* ((female (female-p gender))
         (extra-r (if female "" "r"))
         (beginning (beginning-default extra-r firstname)) ; same as formal
         (end (end-default user-firstname)))
    (list beginning end)))

(defun get-address-for-preview (frame-function gender firstname lastname)
  "Depending on FRAME-FUNCTION, return a list containing
      1. the length of the changed greeting
      2. the adjusted greeting as a string
      3. the adjusted farewell as a string."
  (let* ((address (funcall frame-function gender firstname lastname))
         (greeting (nth 0 address))
         (farewell (nth 1 address))
         (greeting-adjusted (org-trim (replace-regexp-in-string "\n" " " greeting)))
         (farewell-adjusted (org-trim (replace-regexp-in-string "\n" " " farewell))))
    (list (length greeting-adjusted) greeting-adjusted farewell-adjusted)))

(defun list-addresses (firstname lastname gender)
  "Return a list of lists, each of which has as its member one of `frame-functions'
and a list of of
1. the length of the changed greeting
2. the adjusted greeting as a string
3. the adjusted farewell as a string."
  (let (result)
    (dolist (fun frame-functions)
      (let ((preview-list (get-address-for-preview fun gender firstname lastname)))
        (push (list fun preview-list) result)))
    result))

(defun greeting-max-string (addresses)
  "Return the length of the longest greeting-string."
  (let ((result 0))
    (dolist (address addresses)
      (let* ((greeting-string (nth 1 (nth 1 address)))
             (greeting-length (length greeting-string)))
        (if (> greeting-length result)
            (setq result greeting-length))))
    result))

(defun fill-up-string (string maxlength)
  "Fill up STRING up to MAXLENGTH and return the result.
              This function assumes that the STRING's length is smaller than that of MAXLENGTH."
  (let ((difference (- maxlength (length string))))
    (if (equal difference 0)
        string
      (concat string (make-string difference 32))))) ; 32 is space

(defun fill-up-frames (firstname lastname gender)
  "Return filled-up addresses."
  (let* ((addresses (list-addresses firstname lastname gender))
         (maxlength (greeting-max-string addresses)))
    (dolist (address addresses)
      (let* ((greeting (nth 1 (nth 1 address)))
             (greeting-filled (fill-up-string greeting maxlength)))
        (setf (nth 1 (nth 1 address)) greeting-filled))) ; greeting would not work because it is not a place but a local variable
    addresses))

(defun frame-previews (firstname lastname gender)
  "Return a list of previews, filled up correctly."
  (let ((promptlist))
    (dolist (frame (fill-up-frames firstname lastname gender))
      (let* ((frame-function (nth 0 frame))
             (info (nth 1 frame))
             (greeting (nth 0 (cdr info)))
             (farewell (nth 1 (cdr info)))
             (preview (concat greeting "     [...]     " farewell)))
        (push (list frame-function preview) promptlist)))
    promptlist))

(defun ensure-addressbook ()
  (unless (file-exists-p addressbook)
    (with-temp-buffer
      (insert "Mail Address,First Name,Last Name,Gender,Preview,Frame Function\n,,,,,")
      (write-file addressbook))))

(defun extract-previews (previews-and-functions)
  "On the grounds of PREVIEWS-AND-FUNCTIONS,
        return a list of all address previews."
  (let (result)
    (dolist (address previews-and-functions)
      (push (nth 1 address) result))
    result))

(defun frame-function-from-preview (preview previews-and-functions)
        "Return that frame-function in PREVIEWS-AND-FUNCTIONS which produces PREVIEW, as a string."
  (prin1-to-string (car (rassoc (list preview) previews-and-functions))))

(defun ensure-recipient-info ()
  "Get all information needed to automatically insert a greeting.
If not found by automatic search, prompt for them.
Return a list of the recipient's mail address, firstname, lastname, gender, and the a preview of the frame."
  (let* ((firstname (recipient-firstname))
         (lastname (recipient-lastname))
         (gender (ask-for-gender firstname lastname))
         (mail (get-recipient-mail))
         (previews-and-functions (frame-previews firstname lastname gender))
         (previews (extract-previews previews-and-functions))
         (preview-single (completing-read "How would you like to address the recipient?"
                                          previews))
         (frame-function (frame-function-from-preview
                            preview-single
                            previews-and-functions)))

                  ;;; frame function herausfinden anhand von preview und returnen!
    (list mail firstname lastname gender preview-single frame-function)))

(defun parse-csv-line (line)
  "Parse a line of CSV and return a list of values.
This function handles quotation marks correctly."
  (let ((values '())
        (current-value "")
        (inside-quotes nil))
    (dotimes (i (length line))
      (let ((char (substring line i (1+ i))))
        (cond
         ((and (equal char "\"") (not inside-quotes))
          (setq inside-quotes t))
         ((and (equal char "\"") inside-quotes)
          (setq inside-quotes nil))
         ((and (equal char ",") (not inside-quotes))
          (push (if (string= current-value "") nil current-value) values)
          (setq current-value ""))
         (t
          (setq current-value (concat current-value char))))))
    (push (if (string= current-value "") nil current-value) values)
    (reverse values)))

(defun find-specifics (mail)
  "Extract the information belonging to MAIL from `addressbook'. Return a list of
  1. the mail address
  2. the first name
  3. the last name
  4. the gender
  5. the preview of the frame
  6. the frame function to use.
  If MAIL is not found, return nil."
  (with-temp-buffer
    (insert-file-contents addressbook)
    (when (re-search-forward (regexp-opt (list mail)) nil t) ; if you find the mail address
      (let* ((beg (progn (beginning-of-line) (point)))
             (end (progn (end-of-line) (point)))
             (info-in-csv-format (buffer-substring-no-properties beg end)))
        (parse-csv-line info-in-csv-format)))))

(defun find-and-save-recipient-info ()
  "Get all the infos needed in order to address the recipient,
save it in `addressbook' and return it."
  (let ((info (ensure-recipient-info)))
    (with-temp-buffer
      (insert-file-contents addressbook)
      (goto-char (point-max))
      (newline)
      (dolist (element (butlast info))
        (insert (concat "\"" element "\"" ",")))    ; all elements except the last one with comma
      (insert (concat "\"" (car (last info)) "\"")) ; the last element, without a comma
      (write-file addressbook))
    info))

(defun get-recipient-info (mail)
  "Return a list of information associated with
the recipient whose mail address is MAIL.
The list contains of
  1. MAIL address
  2. first name
  3. last name
  4. gender
  5. frame-preview
  6. the frame function to use.
Use the file whose path is (the value of) `addressbook'.
If it does not exist, create it.
If the file exists but the recipient's information is not found,
prompt the user to enter them and store them accordingly."
  (ensure-addressbook)
  (or (find-specifics mail)
      (find-and-save-recipient-info)))

(defun insert-frame ()
  "Insert a greeting at point."
  (interactive)
  (let* ((mail (get-recipient-mail))
         (info (get-recipient-info mail))
         (firstname (nth 1 info))
         (lastname (nth 2 info))
         (gender (nth 3 info))
         (frame-function (intern (nth 5 info))) ; `intern' bc of string
         (frame (funcall frame-function gender firstname lastname))
         (greeting (nth 0 frame))
         (farewell (nth 1 frame)))
    (goto-char (org-msg-start))
    (re-search-forward ":END:" nil t) ; go behind property drawer
    (next-line)
    (beginning-of-line)
    (insert greeting)
    (newline 2)
    (save-excursion
      (insert farewell))
    (previous-line 2)
    (message "Frame (greeting/farewell) inserted automatically")))

(defcustom insert-frame-automatically t
  "Whether or not to automatically insert greeting and farewell wishes when composing a mail.")

(defun insert-frame-automatically ()
  (when insert-frame-automatically
    (beginning-of-line)
    (when (looking-at "^To: ")
      (insert-frame))))

(defun insert-frame-auto (&rest args)
  "In the current mu4e buffer, insert a greeting and farewell clause.
  Automatically fill in name and respect the gender; prompt for information if needed and store it for the next time. See `insert-frame'."
  (if insert-frame-automatically
      (cond ((equal mu4e-compose-type 'reply)
             (insert-frame))
            ((equal mu4e-compose-type 'new)
             (org-msg-get-to-name) ; go to the 'To:' field
             (org-msg-tab)   ; prompt for recipient
             (insert-frame))
            ((equal mu4e-compose-type 'forward)
             nil)
            (t (error "Not composing a mail")))))

(advice-add #'org-msg-post-setup :after #'insert-frame-auto)
(advice-remove #'message-tab  #'insert-frame-automatically)

(provide 'org-msg-greetings)

(defun ensure-subject ()
  (unless (message-fetch-field "subject")
    (error "Your subject field is empty!")))

(defun ensure-time ()
  "Update the date field of the e-mail header to the current date and time."
  (let ((date (message-make-date)))
    (save-excursion
      (save-restriction
        (message-narrow-to-headers)
        (goto-char (point-min))
        (re-search-forward "Date: ")
        (replace-region-contents
         (point)
         (re-search-forward "$")
         (lambda () date))))))

(advice-add #'org-msg-prepare-to-send :before #'ensure-subject)
(advice-add #'org-msg-prepare-to-send :before #'ensure-time)
